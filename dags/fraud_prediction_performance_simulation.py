import airflow
from airflow import DAG
from airflow.operators.bash import BashOperator
from datetime import datetime
from datetime import timedelta
from airflow.models import Variable

# getting the env variable
settings = Variable.get("fraud-detection-simulation", deserialize_json=True)
PROJECT_DIR = settings["PROJECT_DIR"]
env_vars = {'PROJECT_DIR': PROJECT_DIR}

# default arg
default_args = {
    'owner': 'FourthLine',
    'provide_context': True,
    'retries': 1,
    'start_date': datetime(2022, 6, 4, 7, 59, 0),
    'retry_delay': timedelta(minutes=5)
}

# define your dag configuration
dag = DAG(
    dag_id='fraud_detection_simulation_prediction_performance',  # DAG_ID must be unique
    description='Fraud Detection Model Simulation',
    catchup=False,
    max_active_runs=1,
    default_args=default_args
)

# TASK1
input_data_generation = BashOperator(task_id='prediction_performance_simulation',
                                     bash_command='cd $PROJECT_DIR && sh +x run/fraud_prediction_performance_and_save.sh ',
                                     env=env_vars,
                                     dag=dag)
