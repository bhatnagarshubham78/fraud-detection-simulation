import sys
import warnings
import sqlite3
import time
from datetime import datetime
from dateutil.relativedelta import relativedelta

warnings.filterwarnings("ignore")

sys.path.append('../')

from utils.helper_functions import read_json_df, merge_requests_decisions, random_date_list, read_sqlite_table
from config.main_config import sql_uri, events_count, logging, read_prediction_query, decision_dir


def pre_process_output(input_df):
    """

    :param input_df:
    :return:
    """
    output_df = input_df.copy()
    output_df['insert_timestamp'] = random_date_list((datetime.now() - relativedelta(months=+2)), events_count)
    output_df = output_df.rename(columns={'is_fraud_request_cat': 'final_decision_by_agent'})
    output_df = output_df[
        ['insert_timestamp', 'request_id', 'agent_id', 'probability_of_fraud', 'final_decision_by_agent']]
    return output_df


def dump_performance_data(input_df, sql_uri):
    """

    :param sql_uri:
    :param model_dir:
    :param model_name:
    :return:
    """
    output_df = pre_process_output(input_df)
    output_df.drop_duplicates(inplace=True)
    try:
        cnx = sqlite3.connect(sql_uri)
    except Exception as err:
        logging.error('Error connecting database: {}'.format(err))
    else:
        try:
            output_df.to_sql(name='fraud_prediction_performance', con=cnx, index=False, if_exists='append')
        except Exception as err:
            logging.error('Error inserting data into database: {}'.format(err))
        else:
            logging.info('successfully inserted predictions to db')


def process_decisions(df):
    """

    :param df:
    :return:
    """
    df['is_fraud_request_cat'] = df["is_fraud_request"].astype(int)
    df.drop('is_fraud_request', axis=1, inplace=True)
    return df


if __name__ == "__main__":
    start_time = time.time()
    logging.info('******* starting main execution *******')
    logging.info('started fraud performance prediction of requests...')
    requests_df = read_sqlite_table(read_prediction_query)
    decisions_df = read_json_df(decision_dir)
    processed_decisions = process_decisions(decisions_df)
    input_df = merge_requests_decisions([requests_df, processed_decisions])
    dump_performance_data(input_df, sql_uri)
    logging.info('completed fraud performance prediction of requests!')
    logging.info('total execution time: {} minutes'.format((time.time() - start_time) / 60))
