import pandas as pd
import pickle
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
import pprint
import warnings
import time
import sys

sys.path.append('../')
from config.main_config import filter_columns, model_dir, logging, dir_list, modes
from utils.helper_functions import read_json_df, merge_requests_decisions, \
    pre_process_model_input_merge

warnings.filterwarnings("ignore")


def train_test_data_generation(model_input):
    """
    Generates test and train data for model training
    :return:
    """

    # model_input = pre_process_model_input(input_df, filter_columns)
    y_data = model_input['is_fraud_request_cat']
    x_data = model_input.drop('is_fraud_request_cat', axis=1)
    x_training_data, x_test_data, y_training_data, y_test_data = train_test_split(x_data, y_data, test_size=0.2)
    return x_training_data, x_test_data, y_training_data, y_test_data


def train_and_save_model(train_data_x, train_data_y, test_data_x, test_data_y):
    """
    Train and save fraud prediction model in pickle format
    :return:
    """
    model = LogisticRegression()
    model.fit(train_data_x, train_data_y)
    predictions = model.predict(test_data_x)
    classification_report(test_data_y, predictions)
    pprint.pprint(classification_report(test_data_y, predictions))
    pprint.pprint(confusion_matrix(test_data_y, predictions))

    filename = 'finalized_model.sav'
    pickle.dump(model, open(model_dir + filename, 'wb'))


if __name__ == "__main__":
    start_time = time.time()
    logging.info('******* starting main execution *******')
    logging.info('started training prediction model...')
    dataframe_list = [read_json_df(dir_list[i]) for i in range(len(modes))]
    input_df = merge_requests_decisions(dataframe_list)
    model_input = pre_process_model_input_merge(input_df, filter_columns)
    x_training_data, x_test_data, y_training_data, y_test_data = train_test_data_generation(model_input)
    train_and_save_model(x_training_data, y_training_data, x_test_data, y_test_data)
    logging.info('completed training prediction model!')
    logging.info('total execution time: {} minutes'.format((time.time() - start_time) / 60))
