import pickle
import sys
import warnings
import sqlite3
import time
import pandas as pd

warnings.filterwarnings("ignore")

sys.path.append('../')

from utils.helper_functions import read_json_df, pre_process_model_input
from config.main_config import  model_dir, model_name, sql_uri, logging, model_input_col, requests_dir


def pre_process_output(input_df, predictions):
    """
    Pre-process output to merge predictions
    :param input_df:
    :return:
    """
    output_df = input_df.copy()
    output_df['probability_of_fraud'] = predictions.values
    return output_df


def predict_and_dump(request_df, procees_df, sql_uri, model_dir, model_name):
    """
    Predict the fraud probability and save in SQLite DB
    :param sql_uri:
    :param model_dir:
    :param model_name:
    :return:
    """
    loaded_model = pickle.load(open(model_dir + model_name, 'rb'))
    predictions = pd.Series(loaded_model.predict_proba(procees_df)[:, 1])
    output_df = pre_process_output(procees_df, predictions)
    output_df['request_id'] = request_df['request_id']
    output_df.drop_duplicates(inplace=True)
    try:
        cnx = sqlite3.connect(sql_uri)
    except Exception as err:
        logging.error('Error connecting database: {}'.format(err))
    else:
        try:
            output_df.to_sql(name='fraud_prediction', con=cnx, index=False, if_exists='append')
        except Exception as err:
            logging.error('Error inserting data into database: {}'.format(err))
        else:
            logging.info('successfully inserted predictions to db')


if __name__ == "__main__":
    start_time = time.time()
    logging.info('******* starting main execution *******')
    logging.info('started fraud prediction of requests...')
    requests_df = read_json_df(requests_dir)
    processed_df = pre_process_model_input(requests_df, model_input_col)
    predict_and_dump(requests_df, processed_df, sql_uri, model_dir, model_name)
    logging.info('completed fraud prediction of requests!')
    logging.info('total execution time: {} minutes'.format((time.time() - start_time) / 60))
