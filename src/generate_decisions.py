from faker import Faker
import random
from datetime import datetime
from random import Random
from dateutil.relativedelta import relativedelta
import sys
import time

sys.path.append('../')

from utils.helper_functions import random_date, save_json, generate_file_name, generate_file_path
from config.main_config import decision_dir, events_count, past_months_consideration, logging


class DecisionData:
    """
    Class for simulation of operation agents decisions
    """

    def __init__(self, seed_time, seed_value):
        fake = Faker()
        self.seed_time = seed_time
        self.seed_value = seed_value
        self.random_instance = Random(self.seed_value)
        self.request_id = self.random_instance.getrandbits(64)
        self.current_time = datetime.now()
        self.timestamp = random_date(self.seed_time, self.current_time)
        self.agent_id = random.randint(1000, 9999)
        self.is_fraud_request = fake.boolean()

    def format_decision(self):
        """
        Format decisions in the given format
        :return:
        """

        return {
            'timestamp': str(self.timestamp),
            'agent_id': self.agent_id,
            'request_id': self.request_id,
            'is_fraud_request': self.is_fraud_request
        }


def get_decision_data(events_count):
    """
    Generate decisions data
    :param x:
    :return:
    """
    for count in range(0, events_count):
        if count % 1000 == 0:
            logging.info('generated simulations for {} events'.format(count))
        randomized_date = random_date((datetime.now() - relativedelta(months=+past_months_consideration)),
                                      datetime.now())
        decision_instance = DecisionData(randomized_date, count)

        # / decision / year = YYYY / month = MM / day = DD / hour = HH / decision_X.json
        decision_data = decision_instance.format_decision()
        file_path = generate_file_path(data=decision_data, file_dir=decision_dir, mode='decision')
        file_name = generate_file_name(mode='decision')
        full_path = file_path + file_name
        save_json(file_name=full_path, file=decision_data)


def generate_decisions(decision_events_count):
    """
    Simulate decisions generation
    :param decision_events_count:
    :return:
    """
    get_decision_data(decision_events_count)


if __name__ == "__main__":
    start_time = time.time()
    logging.info('******* starting main execution *******')
    logging.info('started simulating decisions...')
    generate_decisions(events_count)
    logging.info('completed simulating decisions!')
    logging.info('total execution time: {} minutes'.format((time.time() - start_time) / 60))
