from faker import Faker
import random
from datetime import datetime
from dateutil.relativedelta import relativedelta
import sys
import time
from random import Random

sys.path.append('../')

from utils.helper_functions import random_date, save_json, generate_file_name, generate_file_path
from config.main_config import requests_dir, events_count, past_months_consideration, s3_bucket_uri, logging


class RequestsData:
    """
    Class for generating account opening requests simulation
    """

    def __init__(self, seed_time, seed_value):
        fake = Faker()
        self.seed_value = seed_value
        self.random_instance = Random(self.seed_value)
        self.request_id = self.random_instance.getrandbits(64)
        self.seed_time = seed_time
        self.current_time = datetime.now()
        self.request_timestamp = random_date(self.seed_time, self.current_time)
        self.latitude = fake.latitude()
        self.longitude = fake.longitude()
        self.document_photo_brightness_percent = random.randint(0, 100)
        self.is_photo_in_a_photo_selfie = fake.boolean()
        self.document_matches_selfie_percent = random.randint(0, 100)
        self.s3_path_to_selfie_photo = s3_bucket_uri + fake.file_name()
        self.s3_path_to_document_photo = s3_bucket_uri + fake.file_name()

    def format_requests(self):
        """
        Format requests in the given format
        :return:
        """
        return {
            'request_id': self.request_id,
            'request_timestamp': str(self.request_timestamp),
            'latitude': float(self.latitude),
            'longitude': float(self.longitude),
            'document_photo_brightness_percent': self.document_photo_brightness_percent,
            'is_photo_in_a_photo_selfie': self.is_photo_in_a_photo_selfie,
            'document_matches_selfie_percent': self.document_matches_selfie_percent,
            's3_path_to_selfie_photo': self.s3_path_to_selfie_photo,
            's3_path_to_document_photo': self.s3_path_to_document_photo
        }


def get_request_data(event_count):
    """
    Generate requests data
    :param event_count:
    :return:
    """
    for count in range(0, event_count):
        if count % 1000 == 0:
            logging.info('generated simulations for {} events'.format(count))
        randomized_date = random_date((datetime.now() - relativedelta(months=+past_months_consideration)),
                                      datetime.now())
        requests_instance = RequestsData(randomized_date, count)

        # / request / year = YYYY / month = MM / day = DD / hour = HH / request_X.json
        requests_data = requests_instance.format_requests()
        file_path = generate_file_path(data=requests_data, file_dir=requests_dir, mode='request')
        file_name = generate_file_name(mode='request')
        full_path = file_path + file_name
        save_json(file_name=full_path, file=requests_data)


def generate_requests(requests_events_count):
    """
    Simulate requests generation
    :param requests_events_count:
    :return:
    """
    get_request_data(requests_events_count)


if __name__ == "__main__":
    start_time = time.time()
    logging.info('******* starting main execution *******')
    logging.info('started simulating requests...')
    generate_requests(events_count)
    logging.info('completed simulating requests!')
    logging.info('total execution time: {} minutes'.format((time.time() - start_time)/60))
