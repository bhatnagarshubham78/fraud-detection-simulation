from random import randrange
from datetime import timedelta
from dateutil.relativedelta import relativedelta
import glob
import sys
import json
from pathlib import Path
from datetime import datetime
from faker import Faker
import sqlite3
import pandas as pd

sys.path.append('../')
from config.main_config import data_dir, events_count, sql_uri


def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    temp_timestamp = start + timedelta(seconds=random_second)
    return temp_timestamp


def save_json(file_name, file):
    """
    Save dict as json file
    :return:
    """
    with open(file_name, 'w') as fp:
        json.dump(file, fp)


def generate_file_path(data, file_dir, mode):
    """
    Generate file path for input data
    :param mode:
    :return:
    """
    if mode == 'decision':
        data_timestamp = data['timestamp']
    elif mode == 'request':
        data_timestamp = data['request_timestamp']
    str_time = str_to_timestamp(data_timestamp)
    str_time_fmt = datetime.strftime(str_time, '%Y/%m/%d/%H/')
    Path(file_dir + str_time_fmt).mkdir(parents=True, exist_ok=True)
    return file_dir + str_time_fmt


def generate_file_name(mode):
    """
    Generate file name for input data
    :param mode:
    :return:
    """
    fake_instance = Faker()
    if mode == 'decision':
        return 'decision_' + fake_instance.md5() + '.json'
    elif mode == 'request':
        return 'request_' + fake_instance.md5() + '.json'


def str_to_timestamp(data):
    """
    Convert str timestamp to datetime
    :param data:
    :return:
    """
    return datetime.strptime(data, '%Y-%m-%d %H:%M:%S.%f')


def read_json_df(file_path):
    """
    Read multiple json files to single dataframe
    :param file_path:
    :return:
    """
    out_df = pd.DataFrame()
    for filename in glob.iglob(file_path + '**/*.json', recursive=True):
        df = pd.read_json(filename, lines=True)
        out_df = out_df.append(df)
    return out_df


def merge_requests_decisions(dataframe_list):
    """
    Merge requests and decisions data
    :param dataframe_list:
    :return:
    """
    joined_df = pd.merge(dataframe_list[0], dataframe_list[1], how='inner', on='request_id')
    joined_df.drop_duplicates(inplace=True)
    joined_df.dropna(inplace=True)
    # joined_df.to_csv(data_dir + 'input_data.csv', index=False)
    return joined_df


def pre_process_model_input_merge(df, filter_col):
    """
    Generates numercial features and filter columns of the merged dataframe
    :param df:
    :return:
    """
    df["is_photo_in_a_photo_selfie_cat"] = df["is_photo_in_a_photo_selfie"].astype(int)
    df["is_fraud_request_cat"] = df["is_fraud_request"].astype(int)
    model_input = df[filter_col]
    return model_input


def pre_process_model_input(df, filter_col):
    """
    Generates numercial features and filter columns of the dataframe
    :param df:
    :return:
    """
    df["is_photo_in_a_photo_selfie_cat"] = df["is_photo_in_a_photo_selfie"].astype(int)
    model_input = df[filter_col]
    return model_input


def read_sqlite_table(sql_query):
    """
    Read data from SQLlite table into pandas dataframe
    :return:
    """

    cnx = sqlite3.connect(sql_uri)
    out_df = pd.read_sql_query(sql_query, cnx)
    return out_df


def random_date_list(start, count):
    """
    This function will return a random datetime between two datetime
    objects.
    """
    dates_list = []
    while count > 0:
        delta = datetime.now() - start
        int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
        random_second = randrange(int_delta)
        temp_timestamp = start + timedelta(seconds=random_second)
        dates_list.append(str(temp_timestamp))
        count = count - 1
    return dates_list


def pre_process_output_merge(input_df, predictions):
    """

    :param input_df:
    :return:
    """
    output_df = input_df.copy()
    output_df['insert_timestamp'] = random_date_list((datetime.now() - relativedelta(months=+2)), events_count)
    output_df = output_df.rename(columns={'is_fraud_request_cat': 'final_decision_by_agent'})
    output_df['probability_of_fraud'] = predictions
    output_df = output_df[
        ['insert_timestamp', 'request_id', 'agent_id', 'probability_of_fraud', 'final_decision_by_agent']]
    return output_df


if __name__ == "__main__":
    timestamp = random_date((datetime.now() - relativedelta(months=+2)), datetime.now())
    print(timestamp)
