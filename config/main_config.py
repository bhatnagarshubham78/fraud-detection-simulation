import os
import sys
from pathlib import Path
import logging

# static logging attributes
log_format = '%(asctime)s | %(levelname)s | %(filename)s | %(message)s'
log_level = logging.INFO

# Logging level
logging.basicConfig(stream=sys.stdout, level=log_level, format=log_format)

home_dir = os.path.abspath(os.path.join(__file__, "../../"))
data_dir = home_dir + "/data/"
log_dir = home_dir + "/logs/"
requests_dir = data_dir + "/request/"
decision_dir = data_dir + "/decision/"
config_dir = home_dir + "/config/"
model_dir = home_dir + "/model/"

Path(requests_dir).mkdir(parents=True, exist_ok=True)
Path(decision_dir).mkdir(parents=True, exist_ok=True)
Path(model_dir).mkdir(parents=True, exist_ok=True)
Path(log_dir).mkdir(parents=True, exist_ok=True)

# previous months count to be considered for events simulation
past_months_consideration = 2
events_count = 10_00_00

s3_bucket_uri = 's3://data-bucket/'
modes = ['request', 'decision']

dir_list = [requests_dir, decision_dir]

filter_columns = ['document_photo_brightness_percent',
                  'is_photo_in_a_photo_selfie', 'document_matches_selfie_percent', 'is_fraud_request_cat',  'latitude', 'longitude']

model_input_col = ['document_photo_brightness_percent', 'is_photo_in_a_photo_selfie_cat',
                   'document_matches_selfie_percent', 'latitude', 'longitude']

model_cols_pred = ['request_id', 'request_timestamp', 'latitude', 'longitude', 'document_photo_brightness_percent',
                   'is_photo_in_a_photo_selfie_cat', 'document_matches_selfie_percent', 's3_path_to_selfie_photo',
                   's3_path_to_document_photo']

model_name = 'finalized_model.sav'

sql_uri = '/Users/multiplier/Documents/sql'

read_prediction_query = """select * from fraud_prediction"""
