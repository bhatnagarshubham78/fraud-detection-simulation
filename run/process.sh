#!/bin/bash

echo "Starting..."
python generate_requests.py
python fraud_prediction_dump.py
python generate_decisions.py
python prediction_performance.py