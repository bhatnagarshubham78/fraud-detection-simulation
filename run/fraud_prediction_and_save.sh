#!/bin/bash

source /Users/multiplier/opt/anaconda3/bin/activate fd

START_TIME=$(date +%s)

# Date format
today_date=$(date +%Y%m%d)

# Code directories
project_dir=$(pwd)
log_dir=${project_dir}"/logs/"

# Make new log directory/ new log file if not present
mkdir -p ${project_dir}/logs
touch ${log_dir}/${today_date}.log

cd ${project_dir}/src/ || exit
python fraud_prediction_dump.py | tee -a ${log_dir}/${today_date}.log

# Check for execution status code
if [[ $? -eq 0 ]]; then
  echo "Prediction Simulation Successful"
else
  echo "Prediction Simulation Failed"
fi

# Run time
END_TIME=$(date +%s)
date
DIFF=$(($END_TIME - $START_TIME))
echo "Run time : " $DIFF
