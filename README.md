# fraud-detection-simulation

## Description
Project covers simulation of a fraud detection environment. Airflow pipelines are created to generate simulated requests for opening a bank account, read the collected request, run a simulated financial fraud prediction model, and store its predictions in SQLite database.
A separate pipeline will simulate operations agents that make the final decision, whether request is fraud or not, based on this recommendation.

## Project Structure

```
|-- config
|-- dags
|-- data
|   |-- decision
|   `-- request
|-- logs
|-- model
|-- notebook
|-- run
|-- src
`-- utils
```

***

## Functionalities present in SRC directory

- **generate_requests.py** - Simulate account opening requests
- **generate_decisions.py** - Simulate operations agents decisions
- **fraud_prediction_dump.py** - Simulate fraud prediction and saving the data to SQLite database
- **train_fraud_prediction.py** - Train fraud prediction model


## Installation
```commandline
cd fraud-detection-simulation/
docker image build -t fraud_sim .
docker run fraud_sim
```

## Usage
### Running pre-trained model
```commandline
cd fraud-detection-simulation/run
sh simulate_requests.sh
sh fraud_prediction_and_save.sh
sh simulate_decisions.sh
sh fraud_prediction_performance_and_save.sh
```
### Training prediction model
```commandline
cd fraud-detection-simulation/run
sh simulate_requests.sh
sh simulate_decisions.sh
sh train_prediction.sh
```

## Roadmap for Production deployment
1. Test Cases implementation
2. Containerization of the code

## Performance Dashbaord
Performance= number_of_correctly_classified_requests / number_of_requests

Last 30 days performance of correctly classified requests is shown below:

## SQL Query for Performance Calculation over last 30 days
```dbn-sql
SELECT 
  strftime('%m-%d', insert_timestamp) AS month_day, 
  COUNT(request_id) AS number_of_requests, 
  SUM(
    CASE WHEN (
      probability_of_fraud > 0.70 
      AND final_decision_by_agent = 1
    ) 
    OR (
      probability_of_fraud <= 0.70 
      AND final_decision_by_agent = 0
    ) THEN 1 ELSE NULL END
  ) AS number_of_correctly_classified_requests, 
  (
    SUM(
      CASE WHEN (
        probability_of_fraud > 0.70 
        AND final_decision_by_agent = 1
      ) 
      OR (
        probability_of_fraud <= 0.70 
        AND final_decision_by_agent = 0
      ) THEN 1 ELSE NULL END
    )* 1.0
  ) / count(request_id) as performance 
FROM 
  fraud_prediction_performance fd 
GROUP BY 
  strftime('%m-%d', insert_timestamp) 
ORDER BY 
  month_day DESC 
LIMIT 
  30
```

![Alt text](/Users/multiplier/Python/fraud-detection-simulation/notebook/dashboard.png)